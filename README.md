# T6_endava
-  1. Implement Delete endpoint for libraries Controller.
- [ ] 2. Add a new resource in the application for books.  Each book should have a reference to a library where it can be found using the library id as a field in book object.
- [ ] 3. Add a controller for the new resource and implement the basic HTTP Methods. 
- [ ] 4. Add business logic that 2 books with the same name cannot be added. Return Bad request with specific message.
- [ ] 5. Add bussiness logic that a book can only be added for and existing library. Otherwise return Not Found with specific message.
- [ ] 6. Add an action to get books by a library. Send the library id as a Query parameter.

