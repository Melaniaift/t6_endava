﻿using System;
namespace BusinessLayer.Models
{
    public class LibraryWriteModel
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string PhysicalAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}
