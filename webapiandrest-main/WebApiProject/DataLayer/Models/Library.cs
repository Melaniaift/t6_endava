﻿using System;
namespace DataLayer.Models
{
    public class Library
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string PhysicalAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}
